﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class player : MonoBehaviour 
{
	public static player Instance { get; private set; }
	public bool IsDead { get { return isDead; } }
	public bool HasStarted { get { return m_started; } }

	[SerializeField] private float m_movementSpeedMax;
	[SerializeField] private float m_movementSmoothTime;
	[SerializeField] private Collider m_levelCollider = null;
	[SerializeField] private GameObject m_projectile = null;
	[SerializeField] private float m_projectileForce = 1000f;
	[SerializeField] private List<GameObject> m_projectileEmitters = null;
	[SerializeField] private float m_firingRate = 1f;

	[SerializeField] private float m_health = 100f;
	[SerializeField] private float m_impactDamage = 20f;
	[SerializeField] private float m_warningLevel = 20f;

	[SerializeField] private GameObject m_colorBody = null;
	[SerializeField] private Color m_color = Color.red;

	[SerializeField] private UnityEngine.UI.Text m_scoreText = null;
	[SerializeField] private Material m_materialToFlash = null;
	[SerializeField] private float m_flashFrequency = 1f;

	[SerializeField] private AudioSource m_shootSound;
	[SerializeField] private AudioSource m_takeHitSound;
	[SerializeField] private AudioSource m_sirenSound;
	[SerializeField] private AudioSource m_dieSound = null;
	[SerializeField] private AudioSource m_powerupCollectSfx = null;

	[SerializeField] GameObject m_model = null;
	[SerializeField] ParticleSystem m_dieEffect = null;

	[SerializeField] private float m_doubleFireRateDuration = 10f;
	[SerializeField] private float m_doubleSpeedDuration = 10f;

	private float m_currentSpeed = 0f;
	private float m_currentSpeedChangeVelocity = 0f;
	private int m_emitterIndex = 0;
	private float m_firingTimer;
	private float m_currentHealth;

	private float m_doubleFireTimer = 0f;
	private float m_doubleSpeedTimer = 0f;

	private int m_score;
	bool isDead = false;
	bool m_started = false;

	private float m_flashTimer = 0f;

	void Awake()
	{
		Instance = this;
		m_currentHealth = m_health;
	}

	// Use this for initialization
	void Start () 
	{
		m_colorBody.renderer.material.color = m_color;
		m_scoreText.text = string.Format ("SCORE : {0}", 0	);
	}

	public void Regen()
	{
		m_currentHealth = m_health;
	}

	public void kickOff()
	{
		m_started = true;
	}

	public void Restart()
	{
		isDead = false;
		m_currentHealth = m_health;
		m_currentSpeed = 0f;
		m_currentSpeedChangeVelocity = 0f;
		m_score = 0;
		m_model.SetActive( true );

		transform.position = Vector3.zero;
		transform.rotation = Quaternion.identity;
	}

	public void EnableDoubleFiring()
	{
		m_doubleFireTimer = m_doubleFireRateDuration;
	}

	public void EnableDoubleSpeed()
	{
		m_doubleSpeedTimer = m_doubleSpeedDuration;
	}
	                       
	// Update is called once per frame
	void Update () 
	{	
		m_scoreText.text = string.Format ("SCORE : {0}", m_score);
	
		if ( isDead || !m_started)
			return;

		m_doubleSpeedTimer -= Time.deltaTime;
		m_doubleSpeedTimer = Mathf.Max (m_doubleSpeedTimer, 0f);


		Vector3 movementDir = getMovementVector();

		float modifiedMaxSpeed = m_doubleSpeedTimer > 0f ? m_movementSpeedMax * 2f : m_movementSpeedMax;
		float targetSpeed = movementDir.sqrMagnitude != 0f ? modifiedMaxSpeed : 0f;

		m_currentSpeed = Mathf.SmoothDamp( m_currentSpeed, targetSpeed, ref m_currentSpeedChangeVelocity, m_movementSmoothTime );

		Vector3 delta = movementDir * m_currentSpeed * Time.deltaTime;

		Vector3 position = transform.position + delta;
		position.x = Mathf.Clamp( position.x, BaddieSpawner.Instance.Bounds.x*-0.5f, BaddieSpawner.Instance.Bounds.x*0.5f );
		position.z = Mathf.Clamp( position.z, BaddieSpawner.Instance.Bounds.y*-0.5f, BaddieSpawner.Instance.Bounds.y*0.5f );
		transform.position = position;

		//look at
		Vector3 lookAtPoint = calcLookAtPoint();
		if ( lookAtPoint != Vector3.zero )
		{
			transform.localRotation	 = Quaternion.LookRotation (lookAtPoint - transform.position);
		}

		m_doubleFireTimer -= Time.deltaTime;
		m_doubleFireTimer = Mathf.Max (m_doubleFireTimer, 0f);


		// fire
		m_firingTimer = Mathf.Max (m_firingTimer - Time.deltaTime, 0f);

		if ( Input.GetMouseButton( 0 ) && m_firingTimer == 0f )
		{
			GameObject projectile = Instantiate( m_projectile, m_projectileEmitters[m_emitterIndex].transform.position, m_projectileEmitters[m_emitterIndex].transform.rotation ) as GameObject;
			projectile.transform.rigidbody.AddForce( transform.forward*m_projectileForce );
			DestroyObject( projectile, 2f );

			m_emitterIndex = (m_emitterIndex + 1) % m_projectileEmitters.Count;
			m_firingTimer = 1f/m_firingRate;

			if ( m_doubleFireTimer > 0f )
			{
				m_firingTimer *= 0.5f;
			}

			m_shootSound.Play();
		}

		// colour
		Color newCol = Color.Lerp( Color.white, m_color, Mathf.Clamp01( m_currentHealth/m_health ) );
		m_colorBody.renderer.material.color = newCol;	

		// flash
		if ( m_currentHealth <= m_warningLevel & m_currentHealth > 0f )
		{
			m_flashTimer += Time.deltaTime * Mathf.PI;
			Color flashCol = Color.Lerp (Color.white, Color.red, Mathf.Abs(Mathf.Cos (m_flashTimer)));
			m_materialToFlash.SetColor ("_SpecColor", flashCol);
			if ( !m_sirenSound.isPlaying )
				m_sirenSound.Play();
		}
		else
		{
			m_materialToFlash.SetColor ("_SpecColor", isDead ? Color.red : Color.white);
			if ( m_sirenSound.isPlaying )
				m_sirenSound.Stop();
		}

	}

	private Vector3 getMovementVector()
	{
		Vector3 direction = Vector3.zero;
		if ( Input.GetKey( KeyCode.W ) )
		{
			direction += Vector3.forward;
		}
		if ( Input.GetKey( KeyCode.S) )
		{
			direction += Vector3.back;
		}
		if ( Input.GetKey( KeyCode.A ) )
		{
			direction += Vector3.left;
		}
		if ( Input.GetKey( KeyCode.D ) )
		{
			direction += Vector3.right;
		}

		return direction;
	}

	private Vector3 calcLookAtPoint( )
	{
		Vector2 mousePos = Input.mousePosition;

		Ray ray = Camera.main.ScreenPointToRay( mousePos );

		RaycastHit hitInfo;
		bool rayCastHit = m_levelCollider.Raycast( ray, out hitInfo, 100f );

		return rayCastHit ? hitInfo.point : Vector3.zero;
	}

	void OnCollisionEnter(Collision col)
	{
		if ( col.collider.gameObject.layer == 12 )
		{
			m_powerupCollectSfx.Play();
			return;
		}
		if ( isDead )
		{
			return;
		}

		m_currentHealth -= m_impactDamage;
		m_takeHitSound.Play();

		if ( m_currentHealth <= 0f )
		{
			m_dieEffect.Play();
			m_model.SetActive( false );
			m_materialToFlash.SetColor ("_SpecColor", Color.red );
			isDead = true;
			m_sirenSound.Stop();
			m_dieSound.Play();
		}
	}

	public void AddScore( int score )
	{
		m_score += score;
	}
}
