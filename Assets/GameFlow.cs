﻿using UnityEngine;
using System.Collections;

public class GameFlow : MonoBehaviour 
{

	[SerializeField] private GameObject m_gameOverText = null;
	[SerializeField] private GameObject m_startText = null;

	private bool m_playing = true;
	private bool m_started = false;

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		if ( !player.Instance.HasStarted )
		{
			if ( Input.GetKeyDown( KeyCode.Space ) ||
			    Input.GetKeyDown( KeyCode.W ) || 
			    Input.GetKeyDown( KeyCode.S ) ||
			    Input.GetKeyDown( KeyCode.A ) ||
			    Input.GetKeyDown( KeyCode.D ))
			{
				// restart
				player.Instance.kickOff();
				BaddieSpawner.Instance.Restart();
				m_startText.SetActive( false );
			}
		}
		else
		{

			if ( player.Instance.IsDead )
			{
				if ( m_playing )
				{
					BaddieSpawner.Instance.StopAllCoroutines();
				}
				m_gameOverText.SetActive( true );
				m_playing = false;

				if ( Input.GetKeyDown( KeyCode.Space ) )
				{
					// restart
					BaddieSpawner.Instance.Restart();
					player.Instance.Restart();
				}
			}
			else
			{
				m_gameOverText.SetActive( false );
				m_playing = true;
			}

		}
	}
}
