﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Baddie : MonoBehaviour 
{
	[SerializeField] private float m_movementSpeed = 1f;
	[SerializeField] private float m_health = 100f;
	[SerializeField] private GameObject m_colorBody = null;
	[SerializeField] private Color m_color = Color.red;
	[SerializeField] private ParticleSystem m_dieEffect = null;
	[SerializeField] private GameObject m_deactivateOnDeath = null;
	[SerializeField] private int m_scoreValue = 100;
	[SerializeField] private AudioSource m_hitEffect = null;
	[SerializeField] private AudioSource m_dieSound = null;

	float m_currentHealth;
	bool isDead = false;

	// Use this for initialization
	void Start () 
	{
		m_currentHealth = m_health;
		m_colorBody.renderer.material.color = m_color;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (player.Instance.IsDead)
			return;

		// home in on player
		Vector3 directionToPlayer = player.Instance.transform.position - transform.position;
		directionToPlayer.y = 0f;
		Vector3 delta = Vector3.Normalize(directionToPlayer) * m_movementSpeed * Time.deltaTime;

		transform.position += delta;
		if ( delta.sqrMagnitude > 0f )
			transform.rotation = Quaternion.LookRotation( delta );
	}

	void OnCollisionEnter(Collision col)
	{
		if ( isDead == true ) 
		{
			return;
		}

		Projectile projectile = col.collider.GetComponent<Projectile> ();
		if ( projectile )
		{
			m_currentHealth -= projectile.getDamage();

			col.collider.gameObject.SendMessage( "onHit" );
			
			Color newCol = Color.Lerp( Color.white, m_color, Mathf.Clamp01( m_currentHealth/m_health ) );
			m_colorBody.renderer.material.color = newCol;

			m_hitEffect.Play();
			
			if ( m_currentHealth <= 0f )
			{
				// die
				kill ();
			}	
		}
		else
		{
			kill();
		}
	}

	void kill()
	{
		StartCoroutine( performDeath() );
	}

	IEnumerator performDeath()
	{
		isDead = true;
		if ( m_dieEffect )
		{
			m_dieEffect.Play();
		}

		m_dieSound.Play();
		player.Instance.AddScore (m_scoreValue);

		CapsuleCollider collider = GetComponent<CapsuleCollider> ();
		collider.enabled = false;

		Destroy( m_deactivateOnDeath );

		while( m_dieEffect.isPlaying )
		{
			yield return null;
		}

		Destroy (gameObject);
	}
}
