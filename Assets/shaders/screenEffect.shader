﻿ 
Shader "UnityCoder/ScreenEffect"
{
Properties {
_MainTex ("Base (RGB)", 2D) = "white" {}
 	
NoiseValue("NoiseValue", Float) = 0
InnerVignetting("InnerVignetting", Float) = 0
OuterVignetting("OuterVignetting", Float) = 0
RandomValue("RandomValue", Float) = 0
TimeLapse("TimeLapse", Float) = 0
 
}
SubShader {
Tags { "RenderType"="Opaque" }
LOD 200
 
CGPROGRAM
#pragma surface surf Lambert noambient novertexlights nolightmap nodirlightmap noforwardadd halfasview
#include "UnityCG.cginc"
#pragma only_renderers opengl
 
sampler2D _MainTex;
 
uniform float NoiseValue;
uniform float InnerVignetting;
uniform float OuterVignetting;
uniform float RandomValue;
uniform float TimeLapse;
 
struct Input {
float2 uv_MainTex;
};
 

float snoise(float2 co){
    return fract(sin(dot(co.xy ,float2(12.9898,78.233))) * 43758.5453);
}
 
void surf (Input IN, inout SurfaceOutput o)
{
 RandomValue = _Time;
 
// Step 1: Convert to grayscale
float3 colour = tex2D(_MainTex,  IN.uv_MainTex.xy).xyz;
float gray = (colour.x + colour.y + colour.z) / 3.0;
float3 grayscale = float3(gray,gray,gray);
 
// Step 2: Appy sepia overlay
float3 finalColour = grayscale;
 
 
// Step 4: Add noise
float t = (1024.0 + RandomValue * 512.0);
float noise = snoise(IN.uv_MainTex.xy * float2(t,t)) * 0.5;
finalColour += noise * NoiseValue;
 

 
// Step 6: Apply vignetting
// Max distance from centre to corner is ~0.7. Scale that to 1.0.
float d = distance(float2(0.5, 0.5), IN.uv_MainTex) * 1.414213;
float vignetting = clamp((OuterVignetting - d) / (OuterVignetting - InnerVignetting), 0.0, 1.0);
finalColour.xyz *= vignetting;
 
 
//half4 c = tex2D (_MainTex, IN.uv_MainTex);
o.Albedo = finalColour;
o.Alpha = 1;
}
ENDCG
}

SubShader {
Tags { "RenderType"="Opaque" }
LOD 200
 
CGPROGRAM
#pragma surface surf Lambert noambient novertexlights nolightmap nodirlightmap noforwardadd halfasview
#include "UnityCG.cginc"
#pragma only_renderers d3d9 flash d3d11_9x d3d11
 
sampler2D _MainTex;
 
uniform float NoiseValue;
uniform float InnerVignetting;
uniform float OuterVignetting;
uniform float RandomValue;
uniform float TimeLapse;
 
struct Input {
float2 uv_MainTex;
};
 
 
void surf (Input IN, inout SurfaceOutput o)
{
 RandomValue = _Time;
 
// Step 1: Convert to grayscale
float3 colour = tex2D(_MainTex,  IN.uv_MainTex.xy).xyz;
float gray = (colour.x + colour.y + colour.z) / 3.0;
float3 grayscale = float3(gray,gray,gray);
 
// Step 2: Appy sepia overlay
float3 finalColour = grayscale;
 
 
// Step 4: Add noise
 
// Step 6: Apply vignetting
// Max distance from centre to corner is ~0.7. Scale that to 1.0.
float d = distance(float2(0.5, 0.5), IN.uv_MainTex) * 1.414213;
float vignetting = clamp((OuterVignetting - d) / (OuterVignetting - InnerVignetting), 0.0, 1.0);
finalColour.xyz *= vignetting;
 
 
//half4 c = tex2D (_MainTex, IN.uv_MainTex);
o.Albedo = finalColour;
o.Alpha = 1;
}
ENDCG
}
FallBack "Diffuse"
}