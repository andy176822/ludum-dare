﻿using UnityEngine;
using System.Collections;

public class powerup : MonoBehaviour {

	public enum Type
	{
		FiringX2,
		SpeedX2,
		Regen,
		Bomb,
	}

	[SerializeField] private Type m_type;
	[SerializeField] private float m_lifetime = 5f;

	// Use this for initialization
	void Start () 
	{
		Destroy (gameObject, m_lifetime);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision col)
	{
		switch( m_type )
		{
		case Type.FiringX2:
			player.Instance.EnableDoubleFiring();
			break;
		case Type.Bomb:
			BaddieSpawner.Instance.killAll();
			break;
		case Type.Regen:
			player.Instance.Regen();
			break;
		case Type.SpeedX2:
			player.Instance.EnableDoubleSpeed();
			break;
		}

		Destroy (gameObject);
	}
}
