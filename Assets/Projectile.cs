﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour 
{
	[SerializeField] private float m_damage = 1f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void onHit()
	{
		Destroy (gameObject);
	}

	public float getDamage() 
	{ 
		return m_damage;
	}
}
