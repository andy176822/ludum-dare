﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class BaddieSpawner : MonoBehaviour 
{
	public static BaddieSpawner Instance { get; private set; }
	public Vector2 Bounds { get { return m_bounds; } }

	[SerializeField] List<GameObject> m_baddies = null;
	[SerializeField] AnimationCurve m_spawnInterval = null;
	[SerializeField] Vector2 m_bounds;
	[SerializeField] float m_minDistanceFromPlayer = 0f;

	[SerializeField] List<GameObject> m_powerups = null;
	[SerializeField] AnimationCurve m_powerupSpawnInterval = null;
	[SerializeField] float m_powerupMinDistanceFromPlayer = 0f;

	float m_runningTime = 0f;

	// Use this for initialization
	void Start () 
	{
		Instance = this;
		//StartCoroutine(spawnBaddies());
	}
	
	// Update is called once per frame
	void Update () 
	{
		m_runningTime += Time.deltaTime;
	}

	public void Restart()
	{
		StopAllCoroutines();
		m_runningTime = 0f;

		foreach(Transform child in transform) {
			Destroy(child.gameObject);
		}

		StartCoroutine(spawnBaddies());
		StartCoroutine(spawnPowerups());
	}

	IEnumerator spawnBaddies()
	{
		while ( true )
		{
			// spawn
			Vector3 spawnLocation = Vector3.zero;
			float distanceFromPlayer = 0f;
			do
			{
				yield return null;	
				spawnLocation.x = Random.Range( m_bounds.x*-0.5f, m_bounds.x*0.5f ); 
				spawnLocation.z = Random.Range( m_bounds.y*-0.5f, m_bounds.y*0.5f ); 
				distanceFromPlayer = (spawnLocation - player.Instance.transform.position).sqrMagnitude;
			} while ( distanceFromPlayer < m_minDistanceFromPlayer*m_minDistanceFromPlayer );

			//spawn it
			int spawnIndex = Random.Range( 0, m_baddies.Count-1 );
			GameObject baddie = Instantiate( m_baddies[ spawnIndex ], spawnLocation, Quaternion.identity ) as GameObject;
			baddie.transform.parent = transform;

			float nextSpawn = 0f;
			nextSpawn = m_spawnInterval.Evaluate( m_runningTime );
			yield return new WaitForSeconds( nextSpawn );
		}
	}

	IEnumerator spawnPowerups()
	{
		yield return new WaitForSeconds( m_powerupSpawnInterval.Evaluate( m_runningTime ) );
		while ( true )
		{
			// spawn
			Vector3 spawnLocation = Vector3.zero;
			float distanceFromPlayer = 0f;
			do
			{
				yield return null;	
				spawnLocation.x = Random.Range( m_bounds.x*-0.5f, m_bounds.x*0.5f ); 
				spawnLocation.z = Random.Range( m_bounds.y*-0.5f, m_bounds.y*0.5f ); 
				distanceFromPlayer = (spawnLocation - player.Instance.transform.position).sqrMagnitude;
			} while ( distanceFromPlayer < m_powerupMinDistanceFromPlayer*m_powerupMinDistanceFromPlayer );
			
			//spawn it
			int spawnIndex = Random.Range( 0, m_powerups.Count );
			GameObject baddie = Instantiate( m_powerups[ spawnIndex ], spawnLocation, Quaternion.identity ) as GameObject;
			baddie.transform.parent = transform;
			
			float nextSpawn = 0f;
			nextSpawn = m_powerupSpawnInterval.Evaluate( m_runningTime );
			yield return new WaitForSeconds( nextSpawn );
		}
	}

	public void killAll()
	{
		foreach(Transform child in transform) {
			child.gameObject.SendMessage("kill",SendMessageOptions.DontRequireReceiver);
		}
	}
}
